import { hello } from "./hello";
import { Hoge } from "./hoge/Hoge";

addEventListener('load', () => {
  hello();

  const hoge = Hoge.getInstance();
  console.log(hoge.getName());
});
